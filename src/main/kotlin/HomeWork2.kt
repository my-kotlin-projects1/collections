fun main() {
    val loginsAndPasses = hashMapOf<String, String>()
    println("Подготовка данных начата:")
    usersDataInput(loginsAndPasses)
    println("Подготовка данных закончена")
    println(loginsAndPasses)
    println()
    println("Авторизация пользователей:")
    authorizationCheck(loginsAndPasses)
}

fun usersDataInput(someHashMap: HashMap<String, String>): HashMap<String, String> {
    val userNumbers = usersNumberInput()
    for (i in 1..userNumbers) {
        println("Введите логин для пользователя $i: ")
        val login = readln()
        println("Введите пароль для пользователя $i: ")
        val pass = readln()
        someHashMap[login] = pass
    }
    return someHashMap
}

fun usersNumberInput(): Int {
    var n: Int
    do {
        println("Введите количество пользователей: ")
        n = readln().toIntOrNull() ?: 0
        if (n <= 0) println("Неверный ввод, введите число больше 0: ")
    } while (n <= 0)
    return n
}

fun authorizationCheck(someHashMap: HashMap<String, String>) {
    println()
    do {
        println("Введите логин: ")
        val login = readln()
        val userExist = someHashMap.keys.contains(login)
        if (userExist) {
            do {println("Введите пароль: ")
            val pass = readln()
            if (pass == someHashMap[login]) println("Добро пожаловать $login")
            else println("Указан неверный пароль")
            } while (pass != someHashMap[login])
        }
        else (println("Указан неверный логин"))
    } while (!userExist)
}


