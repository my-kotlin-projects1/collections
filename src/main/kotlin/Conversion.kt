fun main() {
    val animals = listOf("zebra", "giraffe", "elephant", "rat")
    val babies = animals
        .map{ unit -> "A baby $unit" }
        .map{ unit -> "$unit, with the cutest little tail ever!"}
    println(animals)
    println(babies)

    val tenDollarWords = listOf("auspicious", "avuncular", "obviate")
    val tenDollarWordLengths = tenDollarWords.map { it.length }
    println(tenDollarWordLengths)

    // <T, R> Iterable<T>.map(transform: (T) -> R): ListR> !!

    val lists = listOf(listOf(1, 2, 3), listOf(4, 5, 6))
    println(lists.flatMap { it })
}

