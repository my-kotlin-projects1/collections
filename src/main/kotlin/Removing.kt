import kotlin.random.Random

fun main() {
    val numbers = MutableList(10) { Random.nextInt(-10, 10) }
    println(numbers)

    // case 1, via intermediate list
    val candidatesToRemove = mutableListOf<Int>()
    numbers.forEach {
        if (it < 0) candidatesToRemove.add(it)
    }
    numbers.removeAll(candidatesToRemove)
    println(numbers)
    println(candidatesToRemove)

    // case 2, via iterator
    val iterator = numbers.iterator()
    while (iterator.hasNext()) {
        val item = iterator.next()
        if (item < 0)
            iterator.remove()
    }
    println(numbers)

    // case 3, via Kotlin removeAll()
    numbers.removeAll { it < 0 }
    println(numbers)
}