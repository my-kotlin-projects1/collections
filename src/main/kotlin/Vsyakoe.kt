fun main() {
    val list = listOf(1, 2, 3, 4, 5)
    val numbers2 = MutableList(8) { it * 2 }

    val mutableList = list.toMutableList()
    mutableList.add(19)

    println(list)
    println(mutableList)
    println(numbers2)

    // типа каст массива цифр в массив стрингов. Кайф. И этот, вывод в строку через forEach.
    val stringList = list.map { "This was a number $it"}
    stringList.forEach { println(it)}

    val mutableStringList = stringList.toMutableList()
    mutableStringList.add("New string item")
    println(mutableStringList)
}