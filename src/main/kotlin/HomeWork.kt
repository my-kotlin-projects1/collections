import kotlin.random.Random

fun main() {
    val n = usersInput()
    val firstList = makeList(n)
    println("$firstList - начальный список")
    val mutableList = firstList.toMutableList()

    // замена каждого четного элемента на значение в 10 раз больше через цикл for
    // я хуй знаю, тут чето не работает
    mutableList.forEach {
        if (it%2 == 0){
            it*10
        }
    }
//    for (i in mutableList){
//        if (i%2 == 0) {
//            it = mutableList[i]*10}
//    }
    println("$mutableList - изменяемый список, каждый четный элемент заменен значением в 10 раз больше")

    // сумма значений через sumOf()
    println("${mutableList.sumOf { i -> i }} - сумма всех элементов в списке")

    // отфльтрованный список, оставлены только положительные числа.
    println("${mutableList.filter { it > 0}} - отфильтровано через filter так, что список содержит только положительные числа")

/*    // замена каждого четного значения на значение в 10 раз больше через listIterator
    val iterator = mutableList.listIterator()
    while (iterator.hasNext()) {
        var value = iterator.next()
        if (value%2 == 0) iterator.set(value*10)
    }
    println(mutableList)

    // замена значения в каждом четном индексе на значение в 10 раз больше
    for (i in 0 until mutableList.size){
        if (i%2 != 0) { mutableList[i] = mutableList[i]*10}
    }
    println("$mutableList - изменяемый список, значение в каждом четном индексе умножено на 10")
 */
}

fun usersInput(): Int{
    var n: Int
    do {
        println("Enter a number that is > 0")
        n = readln().toIntOrNull() ?: 0
        if (n <= 0) println("Wrong input")
    } while (n <= 0)
    return n
}

fun makeList(n: Int): List<Int> {
    return List(n) { Random.nextInt(-5, 5) }
}