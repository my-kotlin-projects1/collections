fun main() {
    val loginsAndPasses = hashMapOf<String, String>()
    println("Подготовка данных начата:")
    usersInput(loginsAndPasses)
    println("Подготовка данных закончена")
    println(loginsAndPasses)
    println()
    println("Авторизация пользователей:")
    for (i in 1..3) {authorizationCheck1(loginsAndPasses)}
}

fun usersInput(someHashMap: HashMap<String, String>): HashMap<String, String> {
    for (i in 1..3) {
        println("Введите логин для пользователя $i: ")
        val login = readln()
        println("Введите пароль для пользователя $i: ")
        val pass = readln()
        someHashMap[login] = pass
    }
    return someHashMap
}

fun authorizationCheck1(someHashMap: HashMap<String, String>) {
    println()
    println("Введите логин: ")
    val login = readln()
    val userExist = someHashMap.keys.contains(login)
    if (!userExist) return println("Указан неверный логин")
    println("Введите пароль: ")
    val pass = readln()
    return when {
        userExist && pass == someHashMap[login] -> println("Добро пожаловать $login")
        !userExist || pass != someHashMap[login] -> println("Указан неверный логин и/или пароль")
        else -> println("")
    }
}