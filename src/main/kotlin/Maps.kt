fun main() {
    val map = mapOf<Int, String>(1 to "2", Pair(2, "2"))
    val mutableMap = mutableMapOf<Int, String>()
    val hashMap = hashMapOf(1 to "one", 2 to "two", 3 to "three")
    val treeMap = sortedMapOf(1 to "one")
    val linkedHashMap = linkedMapOf<Int, Int>()

    val priceList = mutableMapOf(
        "Milk" to 80,
        "Yogurt" to 50,
        "Apples" to 150,
        "Bananas" to 70,
        "Coca-Cola" to 100,
        "Orange juice" to 150,
    )
    priceList["Ice cream"] = 75 // ключа нет, будет добавлен
    priceList["Milk"] = 90 // значение заменится
    priceList.remove("Yogurt")

    val priceListHash = hashMapOf(
        "Milk" to 80,
        "Yogurt" to 50,
        "Apples" to 150,
        "Bananas" to 70,
        "Coca-Cola" to 100,
        "Orange juice" to 150,
    )

    val priceListSorted = sortedMapOf(
        "Milk" to 80,
        "Yogurt" to 50,
        "Apples" to 150,
        "Bananas" to 70,
        "Coca-Cola" to 100,
        "Orange juice" to 150,
    )
    println(priceList.keys)
    println(priceList.values.maxOrNull())
    val maxValue = priceList.values.max()
    println(priceList.filterValues { it == maxValue })

    for (entry in priceList.entries){ println("${entry.key} - ${entry.value}")}

    priceList.forEach(){ entry -> println("${entry.key} - ${entry.value}")}
    priceList.forEach(){ (key, value) -> println("$key - $value")}

    val sortedMap2 = priceList.toSortedMap(compareBy<String> { it.length }.thenBy { it } )
    println(sortedMap2)

    println(priceList) // ключи в порядке добавления
    println(priceListHash) // ключи в порядке по хэшкоду
    println(priceListSorted) // ключи - в натуральном порядке для стринга - по алфавиту

    // поменять местами K-V в мапе.
    val gradesByStudent = mapOf("Josh" to 4.0, "Alex" to 2.0, "Jane" to 3.0)
    fun flipValues(map: Map<String, Double>): Map<Double, String> {
        return map.map { it -> Pair(it.value, it.key) }.toMap()
    }

    println(flipValues(gradesByStudent))

}