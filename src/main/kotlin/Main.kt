fun main() {
    val list = listOf(1, 2, 3, 4)
    val mutableList = mutableListOf(1, 2, 3, 4)
    var carList = listOf<String>("Mercedes", "BMW", "Ferrari")
    var mutableCarList = mutableListOf<String>("Mercedes", "BMW", "Ferrari")

    val emptyList = listOf<String>()
    val emptyList2 = emptyList<String>()

    //заполнение списка через лямбда-выражение
    val list2 = List<Int>(5) { 0 }
    val mutableList2 = MutableList<Int>(4) { 1 }
    val carList2 = List<String>(6) { index -> "Car №$index"}
    val mutableCarList2 = List<String>(6) { index -> "Car №$index"}

    println(mutableList2)
    println(carList2)

    val valuesToAdd = listOf(1, 18, 73, 3, 44, 6, 1, 33, 2, 22, 5, 7)
    fun listModify(list: List<Int>): List<Int> {
        val num1 = list.filter { it > 4 }
        println(num1)
        val num2 = num1.windowed(2, 2).map { (one, two) -> "[$one , $two]" }
        println(num2)
        val num3 = num1.windowed(2, 2).map { (one, two) -> one * two }
        println(num3)
        val num4 = num3.fold(0){
            accumulator, number ->
            accumulator + number
        }
        println(num4)
        return num1
    }
    listModify(valuesToAdd)
}