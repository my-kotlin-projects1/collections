fun main() {
    val list = listOf(1, 2, 3, 4, 5)
    val mutableList = mutableListOf<Int>(1, 2, 3, 4, 5)
    var carList = listOf<String>("Mercedes", "BMW", "Ferrari")

    println(mutableList)
    println(mutableList.size)
    mutableList[1] = 4
    mutableList.add(6)
    mutableList.add(7)
    println(mutableList)
    println(mutableList.joinToString(" / "))
    println(mutableList.size)
    println(list[2])
    println(list.lastIndex) // номер индекса
    println(list[list.lastIndex]) // значение в этом индексе

    for (i in list.indices){
        println(list[i] * i)
    }

    list.forEach { item -> println(item + 2) }
    list.forEachIndexed { index, i -> println(i * index) }
    println(list)

    println(list.sumBy {it * 5})
    // считает сумму символов айтемов в массиве строк
    println(carList.sumBy { it.length})
}